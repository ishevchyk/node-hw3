import Profile from "../components/UserProfile/Profile";

const ProfilePage = () => {
  return (
    <Profile />
  );
};

export default ProfilePage;
