import {useContext} from "react";
import AuthContext from "../store/auth-context";
import React from "react";
import AddTruck from "../components/Dashboard/AddTruck";
import AddLoad from "../components/Dashboard/AddLoad";
import Trucks from "../components/Dashboard/Trucks";
import Loads from "../components/Dashboard/Loads";


const Dashboard = () => {
  const {user} = useContext(AuthContext)
  return (
    <React.Fragment>
      {user.role === 'DRIVER' ? <AddTruck/> : <AddLoad/>}
      {user.role === 'DRIVER' ? <Trucks/> : <Loads/>}
    </React.Fragment>
  );
};

export default Dashboard;
