import Card from "../UI/Card/Card";
import {useContext} from "react";
import AuthContext from "../../store/auth-context";
import classes from './Profile.module.css'
// import {useState} from "react";



const Profile = () => {
  const {user} = useContext(AuthContext)
  return (
    <Card className={classes.profile}>
    <h2>Yours profile info</h2>
    <ul>
      <li>Email: {user.email}</li>
      <li>Role: {user.role}</li>
      <li>Registered date: {user.createdDate}</li>
    </ul>
      <button>Change password</button>

    </Card>
  );
}

export default Profile
