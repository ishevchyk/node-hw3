import React, {useContext} from 'react';

import classes from './Navigation.module.css';
import {Link} from "react-router-dom";
import AuthContext from "../../store/auth-context";

const Navigation = () => {
  const authCtx = useContext(AuthContext)
  return (
    <nav className={classes.nav}>
      <ul>
        {authCtx.isLoggedIn && <li className={classes['nav-links']}>
            <Link to={'/me'} style={{ textDecoration: 'none', color: '#0ee0e0'}}>
            Profile
          </Link>
          </li>}
        {authCtx.isLoggedIn && <li className={classes['nav-links']}>
          <Link to={'/dashboard'} style={{ textDecoration: 'none', color: '#0ee0e0'}}>
            Dashboard
          </Link>
        </li>}
        {authCtx.isLoggedIn && <li className={classes['nav-links']}>
          <button onClick={authCtx.logout} className={classes['nav-logout']}>Log out</button>
        </li>}
        {!authCtx.isLoggedIn && <li className={classes['nav-links']}>
          <a href={'/login'}>
            {/*<Link to={'/login'}>*/}
              Log in
            {/*</Link>*/}
          </a>
        </li>}
        {!authCtx.isLoggedIn && <li className={classes['nav-links']}>
          <a href={'/register'}>
            {/*<Link to={'/register'}>*/}
              Sign up
            {/*</Link>*/}
          </a>
        </li>}
      </ul>
    </nav>
  );
};

export default Navigation;
