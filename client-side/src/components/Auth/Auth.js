import classes from './Auth.module.css'
import React, {useContext} from "react";
import {Link} from "react-router-dom";
  import Card from "../UI/Card/Card";
import AuthContext from "../../store/auth-context";


const Auth = (props) => {
  const authCtx = useContext(AuthContext)

  return (
    <React.Fragment>
      <Card className={classes.signup}>
          <h2>Welcome to Epam Freight!</h2>
          <h5>Taking logistics in a new direction with an advanced platform for both shippers and carriers.</h5>
        {!authCtx.isLoggedIn && <button className={classes['signup-btn']}>
            <Link to={'/register'} style={{ textDecoration: 'none', color: '#0ee0e0'}}>
              Sign Up for Free!
            </Link>
          </button>}
        {authCtx.isLoggedIn && <button className={classes['dashboard']}>
          <Link to={'/dashboard'} style={{ textDecoration: 'none', color: '#0ee0e0'}}>
            Go to your dashboard!
          </Link>
        </button>}
      </Card>
      {!authCtx.isLoggedIn && <section className={classes.login}>
        <p>Already have an account?</p>
        <button><Link to={'/login'} style={{ textDecoration: 'none', color: '#0ee0e0'}}>Sign in</Link></button>
      </section>}
</React.Fragment>
  )
}
export default Auth
