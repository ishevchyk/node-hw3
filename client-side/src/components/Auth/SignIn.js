import React, { useRef, useContext} from 'react';

import classes from './SignIn.module.css';
import AuthContext from "../../store/auth-context";
import {Link, useNavigate} from "react-router-dom";
import Card from "../UI/Card/Card";

const SignIn = () => {
  const navigate = useNavigate()

  // const history = useHistory()

  const emailInputRef = useRef();
  const passwordInputRef = useRef();

  const authCtx = useContext(AuthContext)
  // const [isLogin, setIsLogin] = useState(true);



  const loginUser = async (enteredData) => {
    try {
      const res = await fetch('http://localhost:8080/api/auth/login/', {
        method: 'POST',
        body: JSON.stringify(enteredData),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      const data = await res.json()
      await authCtx.login(data.jwt_token)
      await navigate('/', {replace: true})
    } catch (err) {
      console.log(err)
    }

  }

  const submitHandler = (event) => {
    event.preventDefault();

    const enteredData = {
      email: emailInputRef.current.value,
      password: passwordInputRef.current.value
    }

    loginUser(enteredData)

  }



  return (
      <React.Fragment>
        <Card className={classes['sign-in']}>
          <h2>Sign in to your account</h2>
          <form onSubmit={submitHandler} className={classes['signin-form']}>
            <div className={classes['signin-form__inputs']}>
              <div className={classes['signin-form__input']}>
                <label>Email</label>
                <input type="email"
                       ref={emailInputRef}/>
              </div>
              <div className={classes['signin-form__input']}>
                <label>Password</label>
                <input type="password"
                       ref={passwordInputRef}/>
              </div>
            </div>
            <div className={classes['signin-form__actions']}>
              <button>Sign In</button>
            </div>
          </form>
          <p>Forgot your password?</p>
        </Card>
        <div className={classes.signup}>
          <p>Doesn't have an account?</p>
          <button><Link to={'/register'}>Sign up</Link></button>
        </div>
      </React.Fragment>
  );
};

export default SignIn;
