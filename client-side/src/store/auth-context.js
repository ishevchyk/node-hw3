import React, {useCallback, useEffect, useState} from "react";

const AuthContext = React.createContext({
  token: '',
  isLoggedIn: false,
  login: (token) => {},
  logout: () => {},
  user: null
})

const retrieveStoredToken = () => {
  const storedToken = localStorage.getItem('token');
  return {
    token: storedToken,
  };
}


export const AuthContextProvider = (props) => {
  const tokenData = retrieveStoredToken();

  let initialToken;
  if(tokenData) initialToken = tokenData.token;

  const [token, setToken] = useState(initialToken);
  const [user, setUser] = useState(null);

  const userIsLoggedIn = !!token;

  const logoutHandler = useCallback( () => {
    setToken(null);
    localStorage.removeItem('token');
  }, []);

  const loginHandler = (token) => {
    setToken(token);
    localStorage.setItem('token', token);
  };

  const getUser = async () => {
    try {
      const res = await fetch('http://localhost:8080/api/users/me/', {
        headers: {
          'Content-Type': 'application/json',
          'authorization': `Bearer ${localStorage.getItem('token')}`
        }
      });
      if (!res.ok) {
        throw new Error('Something went wrong...')
      }
      const data = await res.json()
      setUser(data.user)
    } catch (err) {
        console.log(err)
    }
  }

  useEffect(() => {
    if(userIsLoggedIn) getUser()
  }, [userIsLoggedIn])

  const contextValue = {
    token: token,
    isLoggedIn: userIsLoggedIn,
    login: loginHandler,
    logout: logoutHandler,
    user: user
  }




  return (
    <AuthContext.Provider value={contextValue}>
      {props.children}
    </AuthContext.Provider>
  )
};

export default AuthContext;
