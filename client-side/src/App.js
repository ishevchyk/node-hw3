import React, {useContext} from "react";
// import AuthContext from "./store/auth-context";
import Layout from "./components/Layout/Layout";
import {Routes, Route, Navigate} from 'react-router-dom';

import AuthPage from "./pages/AuthPage";
import SignIn from "./components/Auth/SignIn";
import SignUp from "./components/Auth/SignUp";
import ProfilePage from "./pages/ProfilePage";
import AuthContext from "./store/auth-context";
import Dashboard from "./pages/Dashboard";


function App() {
  const authCtx = useContext(AuthContext)
  return (
    <Layout>
      <Routes>
        <Route path='/' element={<AuthPage/>} exact/>
        <Route path='/register' element={<SignUp/>}/>
        <Route path='/login' element={<SignIn/>}/>
        <Route path='/me' element={authCtx.isLoggedIn ? <ProfilePage/> : <Navigate to='/'/>}/>
        <Route path='/dashboard' element={<Dashboard/>}/>


      </Routes>
    </Layout>

  );
}

export default App;
